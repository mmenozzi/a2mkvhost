Webgriffe Apache Virtual Host Generator
======================================

Installation
-----------

Open a shell and run the following commands:

	$ cd ~/.local/scripts
	$ git clone git@bitbucket.org:mmenozzi/a2mkvhost.git
	$ sudo ln -s ~/.local/scripts/a2mkvhost/a2mkvhost /usr/local/bin/a2mkvhost
